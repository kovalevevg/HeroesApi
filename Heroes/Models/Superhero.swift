//
//  Hero.swift
//  Heroes
//
//  Created by Kovalev Evgenii on 31.01.2023.
//

import Foundation

// MARK: - WelcomeElement
struct Superhero: Decodable {
    let name: String
    let images: Images
}

// MARK: - Images
struct Images: Decodable {
    let xs, sm, md, lg: String
}

enum LinkApi: String {
    case heroesApi = "https://cdn.rawgit.com/akabab/superhero-api/0.2.0/api/all.json"
}
