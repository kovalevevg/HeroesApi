//
//  SuperHeroesViewController.swift
//  Heroes
//
//  Created by Kovalev Evgenii on 31.01.2023.
//

import UIKit
import Kingfisher

class SuperHeroesViewController: UICollectionViewController {
    
    private var superheroes: [Superhero] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        fetchSuperheroes()
    }

    // MARK: UICollectionViewDataSource
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        superheroes.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "superhero", for: indexPath) as! CollectionViewCell
        let superhero = superheroes[indexPath.row]
        cell.configure(with: superhero)
        return cell
    }
    
    @IBAction func clearCache(_ sender: UIBarButtonItem) {
        let cache = ImageCache.default
        cache.clearMemoryCache()
        cache.clearDiskCache() {
        }
    }
    
    private func fetchSuperheroes() {
        Task {
            do {
                superheroes = try await NetworkManager.shared.fetchData()
                collectionView.reloadData()
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
}
