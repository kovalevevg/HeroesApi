//
//  NetworkManager.swift
//  Heroes
//
//  Created by Kovalev Evgenii on 31.01.2023.
//

import Foundation

enum NetworkError: Error {
    case invalidURL
    case noData
    case decodingError
}

class NetworkManager {
    static let shared = NetworkManager()
    
    private init() {}
    
    func fetchData() async throws -> [Superhero]  {
        guard let url = URL(string: LinkApi.heroesApi.rawValue) else {
            throw NetworkError.invalidURL
        }
        
        let (data, _) = try await URLSession.shared.data(from: url)
        let decoder = JSONDecoder()
        let heroes: [Superhero]
        
        do {
            heroes = try decoder.decode([Superhero].self, from: data)
        } catch let error {
            print(error.localizedDescription)
            throw NetworkError.decodingError
        }
        return heroes
    }
}
